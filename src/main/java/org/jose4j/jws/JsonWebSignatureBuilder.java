package org.jose4j.jws;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.lang.JoseException;

import java.security.Key;
import java.security.cert.X509Certificate;

public class JsonWebSignatureBuilder
{
    private final JsonWebSignature jws;

    JsonWebSignatureBuilder()
    {
        jws = new JsonWebSignature();
    }

    public JsonWebSignature build()
    {
        return jws;
    }

    public JsonWebSignatureBuilder setAlgorithmConstraints(AlgorithmConstraints algorithmConstraints)
    {
        jws.setAlgorithmConstraints(algorithmConstraints);
        return this;
    }

    public JsonWebSignatureBuilder setAlgorithmConstraints(AlgorithmConstraints.ConstraintType type, String... algorithms)
    {
        jws.setAlgorithmConstraints(new AlgorithmConstraints(type, algorithms));
        return this;
    }

    public JsonWebSignatureBuilder setCompactSerialization(String compactSerialization) throws JoseException
    {
        jws.setCompactSerialization(compactSerialization);
        return this;
    }

    public JsonWebSignatureBuilder setAlgorithmHeaderValue(String alg)
    {
        jws.setAlgorithmHeaderValue(alg);
        return this;
    }

    public JsonWebSignatureBuilder setPayload(String payload)
    {
        jws.setPayload(payload);
        return this;
    }

    public JsonWebSignatureBuilder setPayloadBytes(byte[] payloadBytes)
    {
        jws.setPayloadBytes(payloadBytes);
        return this;
    }

    public JsonWebSignatureBuilder setPayloadCharEncoding(String payloadCharEncoding)
    {
        jws.setPayloadCharEncoding(payloadCharEncoding);
        return this;
    }

    public JsonWebSignatureBuilder setEncodedPayload(String encodedPayload)
    {
        jws.setEncodedPayload(encodedPayload);
        return this;
    }

    public JsonWebSignatureBuilder setKey(Key key)
    {
        jws.setKey(key);
        return this;
    }

    public JsonWebSignatureBuilder setKeyIdHeaderValue(String kid)
    {
        jws.setKeyIdHeaderValue(kid);
        return this;
    }

    public JsonWebSignatureBuilder setObjectHeaderValue(String name, Object value)
    {
        jws.getHeaders().setObjectHeaderValue(name, value);
        return this;
    }

    public JsonWebSignatureBuilder setHeader(String name, String value)
    {
        jws.setHeader(name, value);
        return this;
    }

    public JsonWebSignatureBuilder setCriticalHeaderNames(String... headerNames)
    {
        jws.setCriticalHeaderNames(headerNames);
        return this;
    }

    public JsonWebSignatureBuilder setContentTypeHeaderValue(String cty)
    {
        jws.setContentTypeHeaderValue(cty);
        return this;
    }

    public JsonWebSignatureBuilder setCertificateChainHeaderValue(X509Certificate... chain)
    {
        jws.setCertificateChainHeaderValue(chain);
        return this;
    }

}
