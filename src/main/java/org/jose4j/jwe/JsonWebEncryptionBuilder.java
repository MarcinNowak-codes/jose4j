package org.jose4j.jwe;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.lang.JoseException;

import java.security.Key;
import java.security.cert.X509Certificate;

public class JsonWebEncryptionBuilder
{
    private final JsonWebEncryption jwe;

    public JsonWebEncryptionBuilder()
    {
        this.jwe = new JsonWebEncryption();
    }

    public JsonWebEncryption build()
    {
        return jwe;
    }

    public JsonWebEncryptionBuilder setAlgorithmConstraints(AlgorithmConstraints algorithmConstraints)
    {
        jwe.setAlgorithmConstraints(algorithmConstraints);
        return this;
    }

    public JsonWebEncryptionBuilder setAlgorithmConstraints(AlgorithmConstraints.ConstraintType type, String... algorithms)
    {
        jwe.setAlgorithmConstraints(new AlgorithmConstraints(type, algorithms));
        return this;
    }

    public JsonWebEncryptionBuilder setCompactSerialization(String compactSerialization) throws JoseException
    {
        jwe.setCompactSerialization(compactSerialization);
        return this;
    }

    public JsonWebEncryptionBuilder setAlgorithmHeaderValue(String alg)
    {
        jwe.setAlgorithmHeaderValue(alg);
        return this;
    }

    public JsonWebEncryptionBuilder setPayload(String payload)
    {
        jwe.setPayload(payload);
        return this;
    }

    public JsonWebEncryptionBuilder setKeyIdHeaderValue(String kid)
    {
        jwe.setKeyIdHeaderValue(kid);
        return this;
    }

    public JsonWebEncryptionBuilder setHeader(String name, String value)
    {
        jwe.setHeader(name, value);
        return this;
    }

    public JsonWebEncryptionBuilder setCriticalHeaderNames(String... headerNames)
    {
        jwe.setCriticalHeaderNames(headerNames);
        return this;
    }

    public JsonWebEncryptionBuilder setContentTypeHeaderValue(String cty)
    {
        jwe.setContentTypeHeaderValue(cty);
        return this;
    }

    public JsonWebEncryptionBuilder setCertificateChainHeaderValue(X509Certificate... chain)
    {
        jwe.setCertificateChainHeaderValue(chain);
        return this;
    }

    public JsonWebEncryptionBuilder setObjectHeaderValue(String name, Object value)
    {
        jwe.getHeaders().setObjectHeaderValue(name, value);
        return this;
    }

    public JsonWebEncryptionBuilder setPlaintext(String plaintext)
    {
        jwe.setPlaintext(plaintext);
        return this;
    }

    public void setPlaintext(byte[] plaintext)
    {
        jwe.setPlaintext(plaintext);
    }

    public JsonWebEncryptionBuilder setEncryptionMethodHeaderParameter(String enc)
    {
        jwe.setEncryptionMethodHeaderParameter(enc);
        return this;
    }

    public JsonWebEncryptionBuilder setContentEncryptionAlgorithmConstraints(AlgorithmConstraints contentEncryptionAlgorithmConstraints)
    {
        jwe.setContentEncryptionAlgorithmConstraints(contentEncryptionAlgorithmConstraints);
        return this;
    }

    public JsonWebEncryptionBuilder setKey(Key key)
    {
        jwe.setKey(key);
        return this;
    }

    public JsonWebEncryptionBuilder setPlainTextCharEncoding(String plaintextCharEncoding)
    {
        jwe.setPlainTextCharEncoding(plaintextCharEncoding);
        return this;
    }

    public JsonWebEncryptionBuilder setCompressionAlgorithmHeaderParameter(String zip)
    {
        jwe.setCompressionAlgorithmHeaderParameter(zip);
        return this;
    }

    public JsonWebEncryptionBuilder enableDefaultCompression()
    {
        jwe.enableDefaultCompression();
        return this;
    }

    public JsonWebEncryptionBuilder setContentEncryptionKey(byte[] contentEncryptionKey)
    {
        jwe.setContentEncryptionKey(contentEncryptionKey);
        return this;
    }

    public JsonWebEncryptionBuilder setEncodedContentEncryptionKey(String encodedContentEncryptionKey)
    {
        jwe.setEncodedContentEncryptionKey(encodedContentEncryptionKey);
        return this;
    }

    public JsonWebEncryptionBuilder setIv(byte[] iv){
        jwe.setIv(iv);
        return this;
    }

    public JsonWebEncryptionBuilder setEncodedIv(String encodedIv){
        jwe.setEncodedIv(encodedIv);
        return this;
    }

}
